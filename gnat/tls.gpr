--  SPDX-FileCopyrightText: 2023 Max Reznik <reznikmm@gmail.com>
--
--  SPDX-License-Identifier: Apache-2.0
----------------------------------------------------------------

with "tls_config.gpr";
project Tls is

   for Library_Name use "tls4ada";
   for Library_Version use Project'Library_Name & ".so." & TLS_Config.Crate_Version;

   for Source_Dirs use ("../source");
   for Object_Dir use "../.obj/" & TLS_Config.Build_Profile;
   for Create_Missing_Dirs use "True";
   for Library_Dir use "../.lib";

   type Library_Type_Type is ("relocatable", "static", "static-pic");
   Library_Type : Library_Type_Type :=
     external ("TLS_LIBRARY_TYPE", external ("LIBRARY_TYPE", "static"));
   for Library_Kind use Library_Type;

   package Compiler is
      for Default_Switches ("Ada") use TLS_Config.Ada_Compiler_Switches;
   end Compiler;

   package Binder is
      for Switches ("Ada") use ("-Es"); --  Symbolic traceback
   end Binder;

   package Install is
      for Artifacts (".") use ("share");
   end Install;

end TLS;
