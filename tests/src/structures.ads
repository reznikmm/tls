--  SPDX-FileCopyrightText: 2023 Max Reznik <reznikmm@gmail.com>
--
--  SPDX-License-Identifier: Apache-2.0
----------------------------------------------------------------

with Integer_Vectors;

package Structures with SPARK_Mode is

   type Structure is record
      List  : Integer_Vectors.Vector (4);
      Total : Natural := 0;
   end record;

end Structures;
