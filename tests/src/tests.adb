--  SPDX-FileCopyrightText: 2023 Max Reznik <reznikmm@gmail.com>
--
--  SPDX-License-Identifier: Apache-2.0
----------------------------------------------------------------

with Integer_Vectors;
with Structure_Vectors;

procedure Tests with Spark_Mode is
   Str_Vector : Structure_Vectors.Vector (2);
   Str        : Structure_Vectors.Optional_Element :=
     (Is_Set => True, Value => <>);
begin
   Integer_Vectors.Append (Str.Value.List, 1);
   pragma Assert (Integer_Vectors.Last_Index (Str.Value.List) = 1);
   Structure_Vectors.Push (Str_Vector, Str);
   pragma Assert (Structure_Vectors.Last_Index (Str_Vector) = 1);
   Structure_Vectors.Pop (Str_Vector, Str);

   if Str.Is_Set then
      Integer_Vectors.Clear (Str.Value.List);
   end if;
end Tests;
