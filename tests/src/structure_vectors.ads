--  SPDX-FileCopyrightText: 2023 Max Reznik <reznikmm@gmail.com>
--
--  SPDX-License-Identifier: Apache-2.0
----------------------------------------------------------------

pragma SPARK_Mode (On);

with Structures;
with TLS.Owning_Vectors;

package Structure_Vectors is new TLS.Owning_Vectors (Structures.Structure);
