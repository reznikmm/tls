--  SPDX-FileCopyrightText: 2023 Max Reznik <reznikmm@gmail.com>
--
--  SPDX-License-Identifier: Apache-2.0
----------------------------------------------------------------

pragma SPARK_Mode (On);

with TLS.Trivial_Vectors;

package Integer_Vectors is new TLS.Trivial_Vectors (Positive, Integer, 0);
