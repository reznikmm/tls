--  SPDX-FileCopyrightText: 2023 Max Reznik <reznikmm@gmail.com>
--
--  SPDX-License-Identifier: Apache-2.0
----------------------------------------------------------------

generic
   type Element_Type is private;

package TLS.Owning_Vectors with Spark_Mode is

   type Vector (Capacity : Positive) is limited private
     with Default_Initial_Condition =>
       Last_Index (Vector) = 0 and Type_Invariant (Vector);

   function Last_Index (Self : Vector) return Natural;

   type Optional_Element (Is_Set : Boolean := False) is record
      case Is_Set is
         when True =>
            Value : Element_Type;
         when False =>
            null;
      end case;
   end record;

   procedure Push
     (Self  : in out Vector;
      Value : in out Optional_Element)
     with Pre => Last_Index (Self) < Self.Capacity
                 and then not Value'Constrained
                 and then Type_Invariant (Self),
          Post => not Value.Is_Set
                  and Last_Index (Self) = Last_Index (Self)'Old + 1
                  and Type_Invariant (Self);

   procedure Pop
     (Self  : in out Vector;
      Value : out Optional_Element)
     with Pre => Last_Index (Self) > 0
                 and then not Value.Is_Set
                 and then not Value'Constrained
                 and then Type_Invariant (Self),
          Post => Last_Index (Self) = Last_Index (Self)'Old - 1
                  and Type_Invariant (Self);

   function Type_Invariant (Self : Vector) return Boolean;

private

   type Element_Array is array (Positive range <>) of Optional_Element;
   type Element_Array_Access is access Element_Array;

   type Vector (Capacity : Positive) is limited record
      Length : Natural := 0;
      Data   : Element_Array_Access;
   end record;

   function Last_Index (Self : Vector) return Natural is
     (Self.Length);

   function Type_Invariant (Self : Vector) return Boolean is
     (if Self.Length = 0 then Self.Data = null
      else Self.Data /= null and then
        Self.Data'First = 1 and then
        Self.Data'Last >= Self.Length and then
          (if Self.Data'Last > Self.Length then
            (for all Item of Self.Data (Self.Length + 1 .. Self.Data'Last) =>
                not Item.Is_Set)));

end TLS.Owning_Vectors;
