--  SPDX-FileCopyrightText: 2023 Max Reznik <reznikmm@gmail.com>
--
--  SPDX-License-Identifier: Apache-2.0
----------------------------------------------------------------

with Ada.Unchecked_Deallocation;

package body TLS.Owning_Vectors with Spark_Mode is

   procedure Free is new Ada.Unchecked_Deallocation
     (Element_Array, Element_Array_Access);

   ---------
   -- Pop --
   ---------

   procedure Pop
     (Self  : in out Vector;
      Value : out Optional_Element)
   is
      Length : constant Natural := Last_Index (Self);

      procedure Get (Cell : in out Optional_Element;
                     Value : out Optional_Element)
        with Pre => not Value.Is_Set
                    and then not Cell'Constrained
                    and then not Value.Is_Set
                    and then not Value'Constrained,
         Post => not Cell.Is_Set;

      procedure Get (Cell : in out Optional_Element;
                     Value : out Optional_Element) is
      begin
         Value := Cell;
         Cell := (Is_Set => False);
      end Get;
   begin
      Get (Self.Data (Length), Value);
      Self.Length := Length - 1;

      if Self.Length = 0 then
         Free (Self.Data);
      end if;
   end Pop;

   ----------
   -- Push --
   ----------

   procedure Push
     (Self  : in out Vector;
      Value : in out Optional_Element)
   is
      Length : constant Natural := Last_Index (Self);
   begin
      if Self.Data = null then
         Self.Data := new Element_Array (1 .. Positive'Min (4, Self.Capacity));
      elsif Length >= Self.Data'Last then
         declare
            Last : constant Positive :=
              (if Self.Data'Last < Self.Capacity - Self.Data'Length
               then Self.Data'Last + Self.Data'Length
               else Self.Capacity);

            Next : constant Element_Array_Access :=
              new Element_Array'(1 .. Last => <>);
         begin
            Next (1 .. Self.Length) := Self.Data.all;
            Free (Self.Data);
            Self.Data := Next;
         end;
      end if;

      Self.Length := Length + 1;
      Self.Data (Length + 1) := Value;
      Value := (Is_Set => False);
   end Push;

end TLS.Owning_Vectors;
