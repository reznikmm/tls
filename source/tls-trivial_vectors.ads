--  SPDX-FileCopyrightText: 2023 Max Reznik <reznikmm@gmail.com>
--
--  SPDX-License-Identifier: Apache-2.0
----------------------------------------------------------------

generic
   type Index_Type is range <>;
   type Element_Type is private;
   Default_Element : Element_Type;
package TLS.Trivial_Vectors with Spark_Mode is

   type Vector (Capacity : Index_Type) is private
     with Default_Initial_Condition =>
       Is_Empty (Vector) and Type_Invariant (Vector)
     and Last_Index (Vector) = Index_Type'First - 1;

   function First_Index (Ignore : Vector) return Index_Type is
      (Index_Type'First);

   function Last_Index (Self : Vector) return Index_Type'Base;

   function Is_Empty (Self : Vector) return Boolean is
     (Last_Index (Self) < Index_Type'First);

   function Type_Invariant (Self : Vector) return Boolean with Ghost;

   procedure Append
     (Self  : in out Vector;
      Value : Element_Type)
     with
       Pre  => Last_Index (Self) < Self.Capacity and Type_Invariant (Self),
       Post => Last_Index (Self) = Last_Index (Self)'Old + 1
                 and Type_Invariant (Self);

   function Element (Self : Vector; Index : Index_Type) return Element_Type
     with Pre => Index <= Last_Index (Self) and Type_Invariant (Self);

   procedure Clear (Self : in out Vector)
     with Post => Is_Empty (Self) and Type_Invariant (Self);

private

   type Element_Type_Array is array (Index_Type range <>) of Element_Type;
   type Element_Type_Array_Access is access Element_Type_Array;

   subtype Extended_Index is Index_Type'Base range
     Index_Type'Pred (Index_Type'First) .. Index_Type'Last;

   type Vector (Capacity : Index_Type) is record
      Length : Extended_Index := Index_Type'Pred (Index_Type'First);
      Data   : Element_Type_Array_Access;
   end record;

   function Type_Invariant (Self : Vector) return Boolean is
     (if Self.Length < Index_Type'First then Self.Data = null
          else Self.Data /= null and then
            Self.Data'First = Index_Type'First and then
            Self.Data'Last >= Self.Length);

   function Last_Index (Self : Vector) return Index_Type'Base is (Self.Length);

   function Element (Self : Vector; Index : Index_Type) return Element_Type is
     (Self.Data (Index));

end TLS.Trivial_Vectors;
