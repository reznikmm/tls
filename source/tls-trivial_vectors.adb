--  SPDX-FileCopyrightText: 2023 Max Reznik <reznikmm@gmail.com>
--
--  SPDX-License-Identifier: Apache-2.0
----------------------------------------------------------------

with Ada.Unchecked_Deallocation;

package body TLS.Trivial_Vectors with Spark_Mode is

   procedure Free is new Ada.Unchecked_Deallocation
     (Element_Type_Array, Element_Type_Array_Access);

   ------------
   -- Append --
   ------------

   procedure Append
     (Self  : in out Vector;
      Value : Element_Type)
   is
      function Init_Last return Index_Type;

      function Init_Last return Index_Type is
        (Index_Type'Min (Self.Capacity, Index_Type'First + 3));

   begin
      if Self.Data = null then
         Self.Data := new Element_Type_Array'
           (Index_Type'First .. Init_Last => Default_Element);
      elsif Self.Length = Self.Data'Last then
         declare
            Last : constant Index_Type :=
              (if Self.Data'Last < Self.Capacity - Self.Data'Length
               then Self.Data'Last + Self.Data'Length
               else Self.Capacity);

            Next : constant Element_Type_Array_Access :=
              new Element_Type_Array'
                (Self.Data.all &
                   (Self.Data'Last + 1 .. Last => Default_Element));
         begin
            --  Next (Index_Type'First .. Self.Length) := Self.Data.all;
            Free (Self.Data);
            Self.Data := Next;
         end;
      end if;

      Self.Length := Index_Type'Succ (Self.Length);
      Self.Data (Self.Length) := Value;
   end Append;

   -----------
   -- Clear --
   -----------

   procedure Clear (Self : in out Vector) is
   begin
      Free (Self.Data);
      Self.Length := Index_Type'Pred (Index_Type'First);
   end Clear;

end TLS.Trivial_Vectors;
